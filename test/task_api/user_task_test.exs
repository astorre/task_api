defmodule UserTaskTest do
  use ExUnit.Case

  alias TaskApi.UserTask

  @name "Diego"

  setup_all do
    case Process.whereis(UserTask) do
      pid when pid != nil -> pid |> GenServer.stop
      nil -> nil
    end
    UserTask.start_link([])
    :ok
  end

  setup do
    UserTask.clean_operations(@name)
  end

  test "get all operations" do
    assert [] == UserTask.operations(@name)
  end

  test "get operations count" do
    assert 0 == UserTask.operations_count(@name)
  end

  test "add operation" do
    assert :ok == UserTask.add_operation(@name)
    {_operations, id} = Process.whereis(UserTask) |> :sys.get_state
    assert [%{id: id - 1, name: @name}] == UserTask.operations(@name)
  end

  test "remove operation" do
    for _ <- 1..3, do: UserTask.add_operation(@name)
    [first_task|rest_tasks] = UserTask.operations(@name)
    assert :ok == UserTask.remove_operation(first_task.id)
    assert rest_tasks == UserTask.operations(@name)
  end

  test "clean operations" do
    for _ <- 1..3, do: UserTask.add_operation(@name)
    assert :ok == UserTask.clean_operations(@name)
    assert [] == UserTask.operations(@name)
  end
end
