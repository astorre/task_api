defmodule TaskApiTest do
  use ExUnit.Case
  use Plug.Test

  alias TaskApi.UserTask

  @name "John"
  @opts TaskApi.init({})

  setup_all do
    case Process.whereis(UserTask) do
      pid when pid != nil -> pid |> GenServer.stop
      nil -> nil
    end
    UserTask.start_link([])
    :ok
  end

  setup do
    UserTask.clean_operations(@name)
  end

  test "returns ok" do
    conn = conn(:get, "/#{@name}", "")
           |> TaskApi.call(@opts)

    assert conn.state == :sent
    assert conn.status == 200
    assert Poison.decode!(conn.resp_body) == %{"data" => "#{@name}'s task accepted"}
  end

  test "does not add operation" do
    for _ <- 1..11, do: UserTask.add_operation(@name)

    conn = conn(:get, "/#{@name}", "")
           |> TaskApi.call(@opts)

    assert conn.state == :sent
    assert conn.status == 429
    assert Poison.decode!(conn.resp_body) == %{"error" => "Too many requests to user"}
  end
end
