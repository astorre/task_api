defmodule TaskApi do
  import Plug.Conn
  @moduledoc """
  Request handler module.
  """

  alias TaskApi.UserTask

  @doc """
  Compile-time initialization.
  """
  def init(opts) do
    opts
  end

  @doc """
  Maps request in runtime.
  """
  def call(%Plug.Conn{request_path: "/" <> user} = conn, _opts) do
    count = UserTask.operations_count(user)
    if count < 10 do
      UserTask.add_operation(user)
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(200, Poison.encode!(%{data: "#{user}'s task accepted"}))
    else
      conn
      |> put_resp_content_type("application/json")
      |> send_resp(429, Poison.encode!(%{error: "Too many requests to user"}))
    end
  end
end
