defmodule TaskApi.UserTask do
  use GenServer
  require Logger

  @sleep_time 10_000

  ### GenServer API

  def init(state) do
    id = 1 # operation id
    {:ok, {state, id}}
  end

  def handle_call({:operations, name}, _from, {state, id}) do
    {:reply, state, {Enum.filter(state, &(&1.name == name)), id}}
  end

  def handle_call({:operations_count, name}, _from, {state, id}) do
    {:reply, count_by_name(state, name), {state, id}}
  end

  def handle_cast({:add_operation, name}, {state, id}) do
    Task.async(fn -> run_operation(name, id) end)
    {:noreply, {state ++ [%{id: id, name: name}], id + 1}}
  end

  def handle_cast({:remove_operation, id_rem}, {state, id}) do
    {:noreply, {Enum.reject(state, &(&1.id == id_rem)), id}}
  end

  def handle_cast({:clean_operations, name}, {state, id}) do
    {:noreply, {Enum.reject(state, &(&1.name == name)), id}}
  end

  def handle_info({:DOWN, _ref, :process, _pid, _reason}, {state, id}) do
    {:noreply, {state, id}}
  end
   
  def handle_info(_msg, {state, id}) do
    {:noreply, {state, id}}
  end

  ### Client API / Helper functions

  def start_link(state \\ []) do
    GenServer.start_link(__MODULE__, state, name: __MODULE__)
  end

  def operations(user), do: GenServer.call(__MODULE__, {:operations, user})
  def operations_count(user), do: GenServer.call(__MODULE__, {:operations_count, user})
  def add_operation(user), do: GenServer.cast(__MODULE__, {:add_operation, user})
  def remove_operation(id_rem), do: GenServer.cast(__MODULE__, {:remove_operation, id_rem})
  def clean_operations(user), do: GenServer.cast(__MODULE__, {:clean_operations, user})

  defp run_operation(name, id) do
    Logger.info("#{name}'s operation-#{id} started")
    Process.sleep(@sleep_time)
    remove_operation(id)
    Logger.info("#{name}'s operation-#{id} completed")
  end

  defp count_by_name(state, name) do
    Enum.count(state, &(&1.name == name))
  end
end
